package beater

import (
	"fmt"
	"time"
//	"context"
//	"fmt"
	"log"
//	"time"
    "golang.org/x/net/icmp"
    "golang.org/x/net/ipv4"
    "net"
    "os"
//	"google.golang.org/grpc"
//	"os/exec"
	"github.com/elastic/beats/v7/libbeat/beat"
	"github.com/elastic/beats/v7/libbeat/common"
	"github.com/elastic/beats/v7/libbeat/logp"

	"github.com/summit/ptbeat/config"
)

// ptbeat configuration.
type ptbeat struct {
	done   chan struct{}
	config config.Config
	client beat.Client
}

// New creates an instance of ptbeat.
func New(b *beat.Beat, cfg *common.Config) (beat.Beater, error) {
	c := config.DefaultConfig
	if err := cfg.Unpack(&c); err != nil {
		return nil, fmt.Errorf("Error reading config file: %v", err)
	}

	bt := &ptbeat{
		done:   make(chan struct{}),
		config: c,
	}
	return bt, nil
}

// Run starts ptbeat.
func (bt *ptbeat) Run(b *beat.Beat) error {
	logp.Info("ptbeat is running! Hit CTRL-C to stop it.")

	var err error
	bt.client, err = b.Publisher.Connect()
	if err != nil {
		return err
	}

	ticker := time.NewTicker(bt.config.Period)
//	counter := 1
	for {
		select {
		case <-bt.done:
			return nil
		case <-ticker.C:
			rtt:=ping()
			event := beat.Event{
				Timestamp: time.Now(),
				Fields: common.MapStr{
					"type": b.Info.Name,
					"rtt": rtt,
				},
			}
			bt.client.Publish(event)
			logp.Info("Event sent")
		}
			

	}
}

// Stop stops ptbeat.
func (bt *ptbeat) Stop() {
	bt.client.Close()
	close(bt.done)
}
func ping()float64{
    c, err := icmp.ListenPacket("udp4", "0.0.0.0")
    if err != nil {
        log.Fatal("here",err)
    }
    defer c.Close()
     wm := icmp.Message{
         Type: ipv4.ICMPTypeEcho, Code: 0,
         Body: &icmp.Echo{
             ID: os.Getpid() & 0xffff, Seq: 1,
             Data: []byte("01213456789ABCDEF"),
         },
     }
     wb, err := wm.Marshal(nil)
    if err != nil {
        log.Fatal(err)
    }
    begin:=time.Now();
    if _, err := c.WriteTo(wb, &net.UDPAddr{IP: net.ParseIP("8.8.8.8"),}); err != nil {
        log.Fatal(err)
    }
    rb := make([]byte, 1500)
    n, peer, err := c.ReadFrom(rb)
    if err != nil {
        log.Fatal(err)
    }
    rm, err := icmp.ParseMessage(1, rb[:n])
    if err != nil {
        log.Fatal(err)
    }
    rtt:=time.Since(begin)
    switch rm.Type {
        case ipv4.ICMPTypeEchoReply:
            log.Printf("got reflection from %v time %v", peer,rtt)
        default:
            log.Printf("got %+v; want echo reply time %v", rm,rtt)
    }
    return rtt.Seconds()*1000.00


}
func handleErr(err error, message string) {
	if err != nil {
		log.Fatalf("%s: %v", message, err)
	}
}