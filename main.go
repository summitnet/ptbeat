package main

import (
	"os"

	"github.com/summit/ptbeat/cmd"

	_ "github.com/summit/ptbeat/include"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
